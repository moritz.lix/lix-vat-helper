# li-x vat helper

## Installation

```bash
$ npm i li-x-vat-helper
```

## Usage

```js
const lixVatHelper = require('li-x-vat-helper')
// or
const { getVat, isValidVatId } = require('li-x-vat-helper')
// or
import { getVat, isValidVatId } from 'li-x-vat-helper'
```

### getVat:

returns vat for a vatId

```js
const { getVat } = require('li-x-vat-helper')
// no vatId
await getVat('') // return 0.19
// german vatId
await getVat('DE118475690') // returns 0.19
// eu based, non german vatId
await getVat('IT10792590969') // returns 0
```

### isValidVatId:

returns whether a vatId is valid or not

```js
const { isValidVatId } = require('li-x-vat-helper')
// no vatId
await isValidVatId('') // returns false
// invalid vatId
await isValidVatId('DE11847569') // returns false
// german vatId
await isValidVatId('DE118475690') // true
// non german vatId
await isValidVatId('IT10792590969') //true
```
