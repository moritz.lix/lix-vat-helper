import { makeGetVat } from './tax'
import { isValidVatId } from './validation'
const getVat = makeGetVat(isValidVatId)
const nonEu = 'US'
const germanVatId = 'DE118475690'
const isEuValidVat = 'IT10792590969'
const isEuInvalidVat = 'IT1079259096'
describe('get vat', () => {
  it('non eu countries should not pay vat', async() => {
    const tax = await getVat(nonEu)
    expect(tax).toEqual(0)
  })
  it('german companies should pay vat', async () => {
    const tax = await getVat(germanVatId)
    expect(tax).toEqual(0.19)
  })
  it('european countries without valid vatId should pay tax', async () => {
    const tax = await getVat(isEuInvalidVat)
    expect(tax).toEqual(0.19)
  })
  it('european countries with a valid vatId should not pay vat', async () => {
    const tax = await getVat(isEuValidVat)
    expect(tax).toEqual(0)
  })
})
