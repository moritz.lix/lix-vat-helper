import { isValidVatId } from './validation'
const countryCode = 'IT'
const vatNumber = '10792590969'
describe('validate vat id', () => {
  it('invalid country code should return false', async () => {
    const validated = await isValidVatId(countryCode.substr(1), vatNumber)
    expect(validated).toBe(false)
  })
  it('invalid vat number should return false', async () => {
    const validated = await isValidVatId(countryCode, vatNumber.substr(0, -1))
    expect(validated).toBe(false)
  })
  it('valid vat should return true', async () => {
    const validated = await isValidVatId(countryCode, vatNumber)
    expect(validated).toBe(true)
  })
})
