import { IVatId } from './typings'

export function splitVatInfo(vatId: string): IVatId {
  const countryCode = vatId.substr(0, 2)
  const vatNumber = vatId.substr(2)
  return { countryCode, vatNumber }
}
