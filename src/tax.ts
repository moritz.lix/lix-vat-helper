import { IIsValidVat } from './typings'
import { splitVatInfo } from './vat.helper'

const VAT = 0.19
const NO_VAT = 0
const EU_COUNTRY_CODES = [
  'BE',
  'BG',
  'CZ',
  'DK',
  'DE',
  'EE',
  'IE',
  'EL',
  'ES',
  'FR',
  'HR',
  'IT',
  'CY',
  'LV',
  'LT',
  'LU',
  'HU',
  'MT',
  'NL',
  'AT',
  'PL',
  'PT',
  'RO',
  'SI',
  'SK',
  'FI',
  'SE',
  'UK',
]
function isEuCountry(countryCode: string) {
  return EU_COUNTRY_CODES.indexOf(countryCode) > -1
}
function isGerman(countryCode: string) {
  return countryCode === 'DE'
}
export function makeGetVat(isValidVatId: IIsValidVat) {
  return async function getVat(vatId: string): Promise<number> {
    const { countryCode, vatNumber } = splitVatInfo(vatId)
    // German companies have to pay vat
    if (isGerman(countryCode)) return VAT
    const isEu = isEuCountry(countryCode)
    // Non eu countries do not have to pay vat
    if (!isEu) return NO_VAT
    // Eu countries with a valid vatId do not have to pay vat
    if (await isValidVatId(countryCode, vatNumber)) return NO_VAT
    // Eu countries without a valid vatId have to pay vat
    return VAT
  }
}
