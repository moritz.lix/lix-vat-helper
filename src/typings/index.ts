export interface IValidateVatInfo {
  countryCode: string
  vatNumber: string
  requestDate: string
  valid: boolean
  name: string
  address: string
}
export interface IVatId {
  countryCode: string
  vatNumber: string
}

export interface IVatValidator {
  isValidVatId: (vatId: string) => Promise<boolean>
}
export interface IIsValidVat {
  (countryCode: string, vatNumber: string): Promise<boolean>
}
