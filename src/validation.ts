import { IValidateVatInfo, IVatId } from './typings'
import validate from 'validate-vat'

export async function isValidVatId(
  countryCode: string,
  vatNumber: string
): Promise<boolean> {
  return await new Promise((resolve, reject) => {
    validate(countryCode, vatNumber, (err: any, info: IValidateVatInfo) => {
      if (err) {
        console.error(err.message)
        return resolve(false)
      }
      return resolve(info.valid)
    })
  })
}
