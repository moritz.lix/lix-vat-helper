import { isValidVatId } from './validation'
import { makeGetVat } from './tax'
import { splitVatInfo } from './vat.helper'
const getVat = makeGetVat(isValidVatId)
export { getVat, isValidVatId, splitVatInfo }
